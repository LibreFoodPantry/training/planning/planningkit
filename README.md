# Instructor setup instructions

**This is currently a manual setup process. This will eventually be automated.**

1. Create group for each team. Used LibreFoodPantry/Training/WSU Spring 2023 Practice/Team N
2. Fork GuestInfoAPI, GuestInfoBackend, GuestInfoFrontend to group, break fork relationship

For every team do the following

1. Create labels at the team subgroup level:

    Title: `Status::Product Backlog`
    Description: `Not yet selected for a sprint.`

    Title: `Status::Sprint Backlog`
    Description: `Selected for the current sprint.`

    Title: `Status::In Process`
    Description: `Currently being worked on. Should be assigned to one or more team members.`
    Title: `Status::Needs Review`
    Description: `Needs review by a team member before being merged and/or marked "Done".`

    Title: `Status::Done`
    Description: `Completed. Waiting for Product Owner review before being closed.`

2. Add a column to the Epics board at the team subgroup level using the `Status:In Process`.
3. Add columns to the Issues board at the team subgroup level, in the following order:

- `Status::Product Backlog`
- `Status::Sprint Backlog`
- `Status::In Process`
- `Status::Needs Review`
- `Status::Done`

4. Create the following epics

    Title: `Change POST /guests to PUT /guests`

    Description:

    ```text
    *This is not a real change that we would want to make. This is just for the purposes of the planning activity.*

    Change the HTTP method for the endpoint used to create a new guest from POST to PUT.
    ```

    Title: `Add a new endpoint to get just the major version of the API`

    Description:

    ```text
    *This is not a real change that we would want to make. This is just for the purposes of the planning activity.*

    Create a new endpoint GET /version/major that will return the major version of the API (the x in x.y.z).
    ```

    Title: `Change the Thea's Pantry background color`

    Description:

    ```text
    *This is not a real change that we would want to make. This is just for the purposes of the planning activity.*

    Change the background color of the Thea's Pantry web UI to `255,184,40`
    ```

2. Arrange the Epic Board with the epics in the following order:
    1. `Add a new endpoint to get just the major version of the API`
    2. `Change POST /guests to PUT /guests`
    3. `Change the Thea's Pantry background color`
    4. Any other pre-existing epics (shouldn't really be any unless you did some other activity with this team subgroup)

