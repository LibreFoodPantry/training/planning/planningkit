FROM alpine
RUN apk update && \
    apk add --no-cache bash git yq glab
WORKDIR /microserviceskit
COPY . ./
ENTRYPOINT ["./deploy.sh"]
